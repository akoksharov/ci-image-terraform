FROM golang as builder

ENV [ LANG=en_US.UTF-8, LANGUAGE=en_US, LC_ALL=en_US.UTF-8 ]

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt update && apt install -y unzip && \
    apt autoremove -y && apt clean -y && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/apt/*

RUN curl -L "https://releases.hashicorp.com/terraform/0.12.13/terraform_0.12.13_linux_amd64.zip" --output terraform.zip && \
    unzip terraform.zip && \
    curl -L "https://github.com/terraform-linters/tflint/releases/download/v0.12.1/tflint_linux_amd64.zip" --output tflint.zip && \
    unzip tflint.zip && \
    rm terraform.zip tflint.zip

RUN go get -v gitlab.com/akoksharov/ci-utils/cmd/assume-role

FROM ubuntu

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt update && apt install -y ca-certificates && \
    apt autoremove -y && apt clean -y && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/apt/*

COPY --from=builder /go/terraform /go/tflint /go/bin/assume-role /usr/local/bin/

